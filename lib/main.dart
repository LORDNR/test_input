import 'package:flutter/material.dart';
import 'package:test_input/main2.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController num1 = TextEditingController();
  TextEditingController num2 = TextEditingController();
  String string = '1';
  EdgeInsets p = const EdgeInsets.only(top: 20);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextFieldNum(
              num: num1,
              keytype: TextInputType.number,
              name: 'num1',
            ),
            Padding(padding: p),
            TextFieldNum(
              num: num2,
              keytype: TextInputType.number,
              name: 'num2',
            ),

            // TextField(
            //   controller: num2,
            //   keyboardType: TextInputType.number,
            //   decoration: InputDecoration(
            //     hintText: 'กรุณากรอก number 2',
            //     fillColor: Colors.white,
            //     filled: true,
            //   ),
            // ),

            TextButton(
              onPressed: () {
                double total =
                    double.parse(num1.text) + double.parse(num2.text);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Main2(total)));
              },
              child: Text('คำนวณ'),
            ),
          ],
        ),
      ),
    );
  }
}

class TextFieldNum extends StatelessWidget {
  const TextFieldNum({
    Key? key,
    required this.num,
    required this.name,
    required this.keytype,
  }) : super(key: key);

  final TextEditingController num;
  final String name;
  final TextInputType keytype;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: num,
      keyboardType: keytype,
      decoration: InputDecoration(
        hintText: 'กรุณากรอก $name',
        fillColor: Colors.white,
        filled: true,
      ),
    );
  }
}
