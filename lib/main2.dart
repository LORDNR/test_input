import 'package:flutter/material.dart';

class Main2 extends StatefulWidget {
  Main2(this.total);
  double total;

  @override
  State<Main2> createState() => _Main2State();
}

class _Main2State extends State<Main2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Text(
        '${widget.total}',
        style: TextStyle(fontSize: 24),
      ),
    );
  }
}
